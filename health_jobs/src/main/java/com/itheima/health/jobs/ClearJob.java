package com.itheima.health.jobs;

import com.itheima.health.common.RedisConst;
import com.itheima.health.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.JedisPool;

import java.util.Set;

/**
 * @Author likepei
 * @Date 2020/1/26 12:04
 * @Version v1.0
 * @Description 清理图片任务类
 */
public class ClearJob {

    @Autowired
    private JedisPool jedisPool;

    /**
     * 定义清理图片的任务
     */
    public void clearImageJob(){
        System.out.println("clearImageJob....");

        //计算redis中两个集合的差值, 获取垃圾图片名称
        Set<String> set = jedisPool.getResource().sdiff(RedisConst.SETMEAL_PIC_RESOURCES, RedisConst.SETMAIL_PIC_DB_RESOURCES);
        for (String imageFileName : set) {
            //删除图片服务器中的图片文件
            QiniuUtils.deleteFile2QiNiu(imageFileName);
            //删除redis中的数据
            jedisPool.getResource().srem(RedisConst.SETMEAL_PIC_RESOURCES, imageFileName);
        }
    }
}
