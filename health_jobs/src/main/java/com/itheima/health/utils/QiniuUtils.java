package com.itheima.health.utils;

import com.qiniu.common.QiniuException;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.util.Auth;

import java.io.IOException;

/**
 * @Author likepei
 * @Date 2020/1/25 10:58
 * @Version v1.0
 * @Description 七牛云工具类
 */
public class QiniuUtils {
    //生成上传凭证，然后准备上传
    public  static String qiniu_img_url_pre = "http://q49bauumv.bkt.clouddn.com/";
    public  static String accessKey = "L-Ie0yUVH35rgl3X9j7RFAdPsdLSq5hxneGqRER3";
    public  static String secretKey = "ZAcufVAu9jEwa8PAI9vlvMw8uiCdWxK6POrOhdrF";
    public  static String bucket = "361-itcast-health";

    /**
     * 删除文件
     * @param filename 服务端文件名
     */
    public static void deleteFile2QiNiu(String filename){
        //构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.region0());
        String key = filename;
        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.delete(bucket, key);
        } catch (QiniuException ex) {
            //如果遇到异常，说明删除失败
            System.err.println(ex.code());
            System.err.println(ex.response.toString());
        }
    }

    //测试上传与删除
    public static void main(String[] args) throws IOException {
        //测试删除
        QiniuUtils.deleteFile2QiNiu("f0a0b3e977d24fcd9f50daa917faadf6");
    }
}

