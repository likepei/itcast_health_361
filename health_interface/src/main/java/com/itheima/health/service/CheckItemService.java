package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.CheckItem;

import java.util.List;

/**
 * @Author likepei
 * @Date 2019/12/30 16:00
 * @Version v1.0
 * @Description 检查项服务接口
 */
public interface CheckItemService {
    /**
     * 获取检查项列表
     * @return
     */
    public List<CheckItem> findAll();

    /**
     * 编辑
     * @param checkItem
     */
    public void edit(CheckItem checkItem);

    /**
     * 基于ID获取检查项
     * @param id
     * @return
     */
    public CheckItem findById(Integer id);

    /**
     * 根据ID,删除检查项
     * @param id
     */
    public void deleteById(Integer id);

    /**
     * 分页查询
     * @param queryPageBean 中有三个属性
     * 属性一 currentPage: 当前页码
     * 属性二 pageSize: 每页显示记录数
     * 属性三 queryString: 查询条件
     */
    public PageResult pageQuery(QueryPageBean queryPageBean);

    /**
     * 新增检查项
     * @param checkItem
     */
    public void add(CheckItem checkItem);
}
