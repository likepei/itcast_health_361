package com.itheima.health.service;

import com.itheima.health.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

/**
 * @Author likepei
 * @Date 2020/1/27 22:41
 * @Version v1.0
 * @Description 预约设置服务接口
 */
public interface OrderSettingService {
    /**
     * 编辑指定日期的可预约人数
     * @param orderSetting
     */
    public void editNumberByDate(OrderSetting orderSetting);
    /**
     * 获取某个月份的设置列表
     * @param date 月份
     * @return 列表包含Map,预约及预约设置信息
     */
    public List<Map> getOrderSettingByMonth(String date);
    /**
     * 添加预约内容
     * @param list 预约设置列表
     */
    public void add(List<OrderSetting> list);
}
