package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.Setmeal;

import java.util.List;

/**
 * @Author likepei
 * @Date 2020/1/25 22:01
 * @Version v1.0
 * @Description 套餐服务接口
 */
public interface SetmealService {

    /**
     * 基于ID, 获取套餐详情
     * @param id
     * @return
     */
    public Setmeal findById(Integer id);

    /**
     * 获取所有套餐的列表
     * @return
     */
    public List<Setmeal> findAll();

    /**
     * 添加体检套餐
     * @param setmeal 体检套餐基本信息
     * @param checkgroupIds 检查组选定列表
     */
    public void add(Setmeal setmeal, Integer[] checkgroupIds);

    /**
     * 分页获取参数数据
     * @param currentPage 当前页码
     * @param pageSize 默认条数
     * @param queryString 查询插件
     */
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);
}
