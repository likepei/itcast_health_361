package com.itheima.health.service;

import com.itheima.health.entity.Result;

import java.util.Map;

/**
 * @Author likepei
 * @Date 2020/1/31 16:34
 * @Version v1.0
 * @Description 预约订单服务接口
 */
public interface  OrderService {
    /**
     * 根据ID查询预约信息, 包括体检人信息,套餐信息
     * @param id
     * @return
     */
    public Result findById4Detail(Integer id);

    /**
     * 保存预约订单
     * @param map 存储表单数据
     * @return
     */
    public Result addOrder(Map<String,String> map);
}
