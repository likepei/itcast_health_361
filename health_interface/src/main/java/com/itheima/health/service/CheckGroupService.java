package com.itheima.health.service;

import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.CheckGroup;

import java.util.List;

/**
 * 检查组服务接口
 */
public interface CheckGroupService {

    /**
     * 获取所有检查组数据
     */
    public List<CheckGroup> findAll();

    /**
     * 编辑检查组
     * @param checkGroup 检查组基本信息
     * @param checkitemIds 检查项选择列表
     */
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds);

    /**
     * 根据检查组ID,获取检查组数据
     * @param id 检查组id
     */
    public CheckGroup findById(Integer id);

    /**
     * 根据检查组ID,获取选中的检查项列表
     * @param id 检查组ID
     */
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

    /**
     * 分页显示检查组
     * @param currentPage   当前页码
     * @param pageSize      默认条数
     * @param queryString   查询条件
     */
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString);

    /**
     * 添加检查组
     * @param checkGroup 检查组基本信息
     * @param checkItemIds 检查项ID列表
     */
    public void add(CheckGroup checkGroup, Integer[] checkItemIds);
}
