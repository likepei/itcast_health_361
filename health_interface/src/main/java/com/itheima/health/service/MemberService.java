package com.itheima.health.service;
/**
 * @Author likepei
 * @Date 2020/2/1 19:36
 * @Version v1.0
 * @Description 会员服务接口
 */
public interface MemberService {

    /**
     * 会员基于手机号登录
     * @param telephone
     */
    void smsLogin(String telephone);
}
