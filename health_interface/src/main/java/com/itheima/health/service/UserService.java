package com.itheima.health.service;

import com.itheima.health.pojo.User;

public interface UserService {

    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     * @return
     */
    boolean login(String username, String password);

    /**
     * 基于用户名 获取User对象
     * @param username 用户名
     * @return
     */
    User findByUsername(String username);
}
