package com.itheima.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.common.MessageConst;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.service.CheckItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author likepei
 * @Date 2019/12/30 16:03
 * @Version v1.0
 * @Description 检查项控制器
 */
@RestController
@Slf4j
@RequestMapping("/checkitem")
public class CheckItemController {

    @Reference
    private CheckItemService checkItemService;

    /**
     * 获取检查项数据
     * 1.调用service,获取检查项列表数据
     * 2.成功,返回正确信息及列表数据
     * 3.失败,返回错误信息
     */
    @PreAuthorize("hasAuthority('CHECKITEM_QUERY')")
    @RequestMapping("/findAll")
    public Result findAll(){
        try {
            List<CheckItem> checkItemList = checkItemService.findAll();
            return new Result(true, MessageConst.QUERY_CHECKITEM_SUCCESS, checkItemList);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConst.QUERY_CHECKITEM_FAIL);
        }
    }

    /**
     * 基于id, 更新检查项数据
     * 1.调用Service, 更新数据
     * 2.成功,返回编辑成功消息
     * 3.失败,返回编辑失败消息
     * @param checkItem
     * @return
     */
    @PreAuthorize("hasAuthority('CHECKITEM_EDIT')")
    @RequestMapping("/edit")
    public Result edit(@RequestBody CheckItem checkItem){
        log.debug("checkItem: {}", checkItem);

        try {
            //调用Service, 更新数据
            checkItemService.edit(checkItem);
            //成功,返回编辑成功消息
            return new Result(true, MessageConst.EDIT_CHECKITEM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            //失败,返回编辑失败消息
            return new Result(false, MessageConst.EDIT_CHECKITEM_FAIL);
        }

    }

    /**
     * 基于ID,获取检查项数据
     * 1.调用Service,基于ID获取检查项对象
     * 2.成功,返回检查项对象数据
     * 3.失败,返回操作失败消息
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('CHECKITEM_QUERY')")
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try {
            //调用Service,基于ID获取检查项对象
            CheckItem checkItem = checkItemService.findById(id);
            //成功,返回检查项对象数据
            return new Result(true, MessageConst.ACTION_SUCCESS, checkItem);
        } catch (Exception e) {
            e.printStackTrace();
            //失败,返回操作失败消息
            return new Result(false, MessageConst.ACTION_FAIL);
        }
    }

    /**
     * 根据ID, 删除检查项
     * 1.调用Service,根据id删除检查项
     * 2.成功,返回删除成功消息
     * 3.失败,返回删除失败消息
     */
    @PreAuthorize("hasAuthority('CHECKITEM_DELETE')")
    @RequestMapping("/delete")
    public Result delete(Integer id){
        log.debug("delete:{}",id);
        try {
            //调用Service 删除检查项
            checkItemService.deleteById(id);
            //成功,返回删除成功消息
            return new Result(true, MessageConst.DELETE_CHECKITEM_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            //失败,返回删除失败消息
            return new Result(false, MessageConst.DELETE_CHECKITEM_FAIL);
        }
    }

    /**
     * 分页查询
     * 1.调用Service获取分页结果数据
     * 2.成功,直接返回有内容的结果
     * 3.失败,返回初始化结果,记录为0,数据为空
     * @param queryPageBean
     * @return
     */
    @PreAuthorize("hasAuthority('CHECKITEM_QUERY')")
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        log.debug("queryBean:{}", queryPageBean);
        try {
            //调用Service获取分页结果数据
            PageResult pageResult = checkItemService.pageQuery(queryPageBean);
            //成功, 直接返回有内容的结果
            return pageResult;
        } catch (Exception e) {
            e.printStackTrace();
            //失败, 返回初始化结果, 记录为0, 数据为空
            return new PageResult(0L, null);
        }
    }

    /**
     *  新增检查项
     *  1.调用Service完成新增业务
     *  2.成功,返回添加成功消息
     *  3.失败,返回添加失败消息
     * @param checkItem
     * @return
     */
    @PreAuthorize("hasAuthority('CHECKITEM_ADD')")
    @RequestMapping("/add")
    public Result add(@RequestBody CheckItem checkItem){
        log.debug("add:{}", checkItem);
        try {
            //调用Service完成新增业务
            if (checkItemService != null){
                checkItemService.add(checkItem);
                //成功,返回添加成功消息
                return new Result(true, MessageConst.ADD_CHECKITEM_SUCCESS);
            } else {
                log.debug("checkItemService is null, 未找到服务对象");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        //失败,返回添加失败消息
        return new Result(false, MessageConst.ADD_CHECKITEM_FAIL);
    }
}
