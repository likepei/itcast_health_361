package com.itheima.health.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.common.MessageConst;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrderSettingService;
import com.itheima.health.utills.POIUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author likepei
 * @Date 2020/1/27 22:48
 * @Version v1.0
 * @Description 预约设置控制器
 */
@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {

    @Reference
    private OrderSettingService orderSettingService;

    /**
     * 基于日期编辑预约设置数据
     * @param orderSetting 预约设置数据
     * @return
     */
    @RequestMapping("/editNumberByDate")
    public Result editNumberByDate(@RequestBody OrderSetting orderSetting){
        try {
            //编辑预约设置
            orderSettingService.editNumberByDate(orderSetting);
            //返回设置成功
            return new Result(true, MessageConst.ORDERSETTING_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            //返回设置失败
            return new Result(false, MessageConst.ORDERSETTING_FAIL);
        }
    }

    /**
     * 获取某月预约数据
     * @param date 月份 yyyy-mm
     * @return
     */
    @RequestMapping("/getOrderSettingByMonth")
    public Result getOrderSettingByMonth(String date){
        try {
            //获取数据
            List<Map> list = orderSettingService.getOrderSettingByMonth(date);
            //返回数据
            return new Result(true, MessageConst.GET_ORDERSETTING_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConst.GET_ORDERSETTING_FAIL);
        }

    }

    /**
     * 提交预约设置的excel文件
     * 步骤:
     * 1.利用POI工具类,从文件附件项中获取Excel原始数据( List<String[]> )
     * 2.把List<String[]>数组列表, 转换为List<OrderSetting>对象列表
     * 3.调用Service, 批量完成OrderSetting数据的保存
     * 4.保存成功,返回成功消息
     * 5.保存失败,返回异常信息
     *
     * @param multipartFile 上传的文件
     */
    @RequestMapping("/upload")
    public Result upload(@RequestParam("excelFile") MultipartFile multipartFile){
        System.out.println("excelFile:" + multipartFile.getOriginalFilename());
        try {
            //利用POI工具类,从文件附件项中获取Excel原始数据( List<String[]> )
            List<String[]> list = POIUtils.readExcel(multipartFile);
            if (list!=null && list.size()>0) {
                //把List<String[]>数组列表, 转换为List<OrderSetting>对象列表
                List<OrderSetting> orderSettingList = new ArrayList<>();
                //从excel遍历数据,封装数据为设置对象,添加到集合
                for (String[] rowData : list) {
                    if(rowData[0] == null){
                        continue;
                    }
                    OrderSetting orderSetting = new OrderSetting(new Date(rowData[0]), Integer.parseInt(rowData[1]));
                    orderSettingList.add(orderSetting);
                }
                //调用Service, 批量完成OrderSetting数据的保存
                orderSettingService.add(orderSettingList);
                //保存成功,返回成功消息
                return new Result(true, MessageConst.IMPORT_ORDERSETTING_SUCCESS);
            } else {
                throw new RuntimeException("数据不能为空");
            }
        } catch (IOException e) {
            e.printStackTrace();
            //保存失败,返回异常信息
            return new Result(false, MessageConst.IMPORT_ORDERSETTING_FAIL);
        }
    }
}
