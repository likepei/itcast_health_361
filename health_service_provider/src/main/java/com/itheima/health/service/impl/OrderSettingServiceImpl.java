package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.dao.OrderSettingDao;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrderSettingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author likepei
 * @Date 2020/1/27 23:31
 * @Version v1.0
 * @Description 预约设置服务接口实现类
 */
@Service
@Transactional
@Slf4j
public class OrderSettingServiceImpl implements OrderSettingService {

    @Autowired
    private OrderSettingDao orderSettingDao;

    /**
     * 基于日期编辑预约设置数据
     * @param orderSetting
     */
    @Override
    public void editNumberByDate(OrderSetting orderSetting) {
        //检查此数据(日期)是否存在
        long count = orderSettingDao.countByOrderDate(orderSetting.getOrderDate());
        if (count> 0) {
            //存在,执行更新操作
            orderSettingDao.updateOrderSettingByOrderDate(orderSetting);
        } else {
            //不存在,执行添加操作
            orderSettingDao.add(orderSetting);
        }
    }

    /**
     * 获取某月的预约数据
     * @param date 月份
     * @return
     */
    @Override
    public List<Map> getOrderSettingByMonth(String date) {
        //月初日期
        String dateBegin = date + "-1"; // yyyy-mm-dd
        //月底日期
        String dateEnd = date+"-31"; // yyyy-mm-dd
        //将月初日期与月底日期存入Map集合
        Map map = new HashMap();
        map.put("dateBegin", dateBegin);
        map.put("dateEnd", dateEnd);

        //获取指定日期范围内的预约数据
        List<OrderSetting> list = orderSettingDao.getOrderSettingByMonth(map);

        //定义用于返回的List集合, 存入预约日期,可预约人数,已预约人数
        List<Map> data = new ArrayList<>();
        for (OrderSetting orderSetting : list) {
            Map<String, Object> orderSettingMap = new HashMap<>();
            //添加 预约日期(几号)
            orderSettingMap.put("date", orderSetting.getOrderDate().getDate());
            //添加 可预约人数
            orderSettingMap.put("number", orderSetting.getNumber());
            //添加 已预约人数
            orderSettingMap.put("reservations", orderSetting.getReservations());
            //添加Map到List集合
            data.add(orderSettingMap);
        }

        return data;
    }

    /**
     * 添加预约设置
     * 步骤:
     * 1.判断数据是否为空,为空抛出异常
     * 2.遍历列表,逐个判断保存
     *  1.如果预约日期存在,执行更新操作
     *  2.如果预约日期不存在,执行保存操作
     *
     * @param list 预约设置列表
     */
    @Override
    public void add(List<OrderSetting> list) {
        //判断数据是否为空,为空抛出异常
        if (list==null || list.size()==0){
            log.debug("list is null");
            throw new RuntimeException("数据不能为空");
        }
        //遍历列表,逐个判断保存
        for (OrderSetting orderSetting : list) {
            //检查此数据(日期)是否存在
            long count = orderSettingDao.countByOrderDate(orderSetting.getOrderDate());
            if (count>0) {
                //存在,执行更新操作
                orderSettingDao.updateOrderSettingByOrderDate(orderSetting);
            } else {
                //不存在,执行保存操作
                orderSettingDao.add(orderSetting);
            }
        }
    }
}
