package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.dao.CheckItemDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.service.CheckItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author likepei
 * @Date 2019/12/30 16:53
 * @Version v1.0
 * @Description 检查项服务接口实现类
 */
@Service
@Transactional
public class CheckItemServiceImpl implements CheckItemService {

    @Autowired
    private CheckItemDao checkItemDao;

    @Override
    public List<CheckItem> findAll() {
        //调用dao,获取检查项
        return checkItemDao.findAll();
    }

    @Override
    public void edit(CheckItem checkItem) {
        //调用dao, 更新检查项
        checkItemDao.edit(checkItem);
    }

    @Override
    public CheckItem findById(Integer id) {
        //调用dao, 获取指定ID的检查项
        return checkItemDao.findById(id);
    }

    @Override
    public void deleteById(Integer id) {
        //根据检查项ID,查询关联的中间表数据,若记录数大于零,不能删除,抛异常
        Long count = checkItemDao.countCheckItemsById(id);
        if(count > 0){
            throw new RuntimeException("当前检查项有数据,不能删除");
        }
        //调用dao,删除指定ID的检查项
        checkItemDao.deleteCheckItemById(id);
    }

    @Override
    public PageResult pageQuery(QueryPageBean pageBean) {
        //使用分页插件PageHelper
        PageHelper.startPage(pageBean.getCurrentPage(),pageBean.getPageSize());
        //获取分页数据
        Page<CheckItem> page = checkItemDao.selectByCondition(pageBean.getQueryString());
        //封装分页数据,返回分页对象
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 新增
     * @param checkItem
     */
    @Override
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);
    }
}
