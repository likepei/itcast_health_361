package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.dao.MemberDao;
import com.itheima.health.pojo.Member;
import com.itheima.health.service.MemberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Author likepei
 * @Date 2020/2/1 20:27
 * @Version v1.0
 * @Description 会员服务接口实现类
 */
@Service
@Slf4j
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberDao memberDao;

    /**
     * 会员手机号登录
     * 步骤:
     * 1.根据手机号获取会员信息
     * 2.若不是会员, 自动注册会员
     * @param telephone
     */
    @Transactional
    @Override
    public void smsLogin(String telephone) {
        //根据手机号获取会员信息
        Member member = memberDao.findByTeleMember(telephone);
        //若不是会员, 自动注册会员
        if (member == null) {
            //自动注册会员
            member = new Member();
            member.setPhoneNumber(telephone);
            member.setRegTime(new Date());
            //调用Service保存会员
            memberDao.add(member);
        }

    }
}
