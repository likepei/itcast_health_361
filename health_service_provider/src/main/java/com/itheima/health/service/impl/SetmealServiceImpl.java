package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.common.RedisConst;
import com.itheima.health.dao.SetmealDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author likepei
 * @Date 2020/1/25 20:13
 * @Version v1.0
 * @Description 套餐业务接口实现类
 */
@Service
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    private SetmealDao setmealDao;
    @Autowired
    private JedisPool jedisPool;

    /**
     * 根据id查询套餐
     * @param id
     * @return
     */
    @Override
    public Setmeal findById(Integer id) {
        return setmealDao.findById(id);
    }

    /**
     * 查询所有套餐
     * @return
     */
    @Override
    public List<Setmeal> findAll() {
        return setmealDao.findAll();
    }

    /**
     * 添加套餐
     * @param setmeal 体检套餐基本信息
     * @param checkgroupIds 检查组选定列表
     */
    @Transactional
    @Override
    public void add(Setmeal setmeal, Integer[] checkgroupIds) {
        //保存套餐数据
        setmealDao.add(setmeal);
        //保存套餐与检查组对应关系
        for (Integer checkgroupId : checkgroupIds) {
            Map<String, Integer> map = new HashMap<>();
            map.put("setmeal_id", setmeal.getId());
            map.put("checkgroup_id", checkgroupId);
            setmealDao.addSetAndCheckGroup(map);
        }
        //保存图片名称到redis数据库
        jedisPool.getResource().sadd(RedisConst.SETMAIL_PIC_DB_RESOURCES, setmeal.getImg());
    }

    /**
     * 分页查询
     * @param currentPage 当前页码
     * @param pageSize 默认条数
     * @param queryString 查询插件
     * @return
     */
    @Override
    public PageResult pageQuery(Integer currentPage, Integer pageSize,String queryString) {
        PageHelper.startPage(currentPage, pageSize);
        Page<Setmeal> pageSetmeals = setmealDao.selectByCondition(queryString);
        return new PageResult(pageSetmeals.getTotal(), pageSetmeals.getResult());
    }
}
