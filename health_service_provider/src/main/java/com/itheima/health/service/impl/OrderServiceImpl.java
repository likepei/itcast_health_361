package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.common.MessageConst;
import com.itheima.health.dao.MemberDao;
import com.itheima.health.dao.OrderDao;
import com.itheima.health.dao.OrderSettingDao;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Member;
import com.itheima.health.pojo.Order;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrderService;
import com.itheima.health.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author likepei
 * @Date 2020/1/31 17:57
 * @Version v1.0
 * @Description 预约服务接口实现类
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderSettingDao orderSettingDao;
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private OrderDao orderDao;

    /**
     * 根据id查询预约信息, 包括体检人信息,套餐信息
     * @param id 订单id
     * @return
     */
    @Override
    public Result findById4Detail(Integer id) {
        try {
            //根据id查询预约信息, 包括体检人信息,套餐信息
            Map<String,Object> map = orderDao.findById4Detail(id);
            map.put("orderDate", DateUtils.parseDate2String((Date) map.get("orderDate")));
            return new Result(true, MessageConst.QUERY_ORDER_SUCCESS, map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConst.QUERY_ORDER_FAIL);
        }
    }

    /**
     * 保存预约
     * 步骤:
     * 1.检查预约日期是否做预约设置
     * 2.检查预约设置日期是否已经约满
     * 3.检查用户是否为会员
     *      是会员,查询是否已经预约
     *      非会员,则自动注册会员
     * 4.更新预约设置当日的已预约人数
     * 5.保存预约订单, 预约成功
     *
     * @param map 存储表单数据
     */
    @Transactional
    @Override
    public Result addOrder(Map<String, String> map) {
        try {
            //检查预约日期是否做预约设置
            log.debug("检查日期是否做过预约设置");
            Date orderDate = DateUtils.parseString2Date(map.get("orderDate"));
            OrderSetting orderSetting = orderSettingDao.findByOrderDate(orderDate);
            if (orderSetting == null){
                return new Result(false, MessageConst.SELECTED_DATE_CANNOT_ORDER);
            }

            //检查预约设置日期是否已经约满
            log.debug("检查预约设置日期是否已经约满");
            //获取可预约人数
            int number = orderSetting.getNumber();
            //获取已预约人数
            int reservations = orderSetting.getReservations();
            if (reservations >= number){
                return new Result(false, MessageConst.ORDER_FULL);
            }

            //检查用户是否为会员
            log.debug("检查用户是否为会员");
            String telephone = map.get("telephone");
            //根据手机号码查询会员
            Member member = memberDao.findByTeleMember(telephone);
            //获取套餐ID
            int setmealId = Integer.parseInt(map.get("setmealId"));
            if (member != null){
                //是会员,查询是否已经预约
                Integer memberId = member.getId();//会员ID
                Order order = new Order(memberId, orderDate, null, null, setmealId);
                //根据套餐ID,会员ID,预约日期进行多条件查询 预约订单信息
                List<Order> orderList = orderDao.findByCondition(order);
                if (orderList !=null && orderList.size() >0){
                    return new Result(false, MessageConst.HAS_ORDERED);
                }
            } else {
                //非会员,则自动注册会员
                member = new Member();
                member.setName(map.get("name")); //姓名
                member.setSex(map.get("sex")); //性别
                member.setPhoneNumber(map.get("telephone"));//电话
                member.setIdCard(map.get("idCard"));//身份证号码
                member.setRegTime(new Date());//注册日期
                memberDao.add(member);
                log.debug("保存会员...");
            }

            //更新预约设置当日的已预约人数
            orderSetting.setReservations(orderSetting.getReservations()+1);
            orderSettingDao.editReservationsByOrderDate(orderSetting);

            //保存预约订单, 预约成功
            Order order = new Order(null,member.getId(), orderDate, map.get("orderType"), Order.ORDERSTATUS_NO, setmealId);
            orderDao.add(order);
            log.debug("保存预约订单...{}", order);
            return new Result(true, MessageConst.ORDER_SUCCESS, order);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConst.ACTION_FAIL);
        }
    }
}
