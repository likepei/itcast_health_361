package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.health.dao.PermissionDao;
import com.itheima.health.dao.RoleDao;
import com.itheima.health.dao.UserDao;
import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import com.itheima.health.pojo.User;
import com.itheima.health.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * @Author likepei
 * @Date 2019/12/28 18:49
 * @Version v1.0
 * @Description 用户业务接口实现类
 */
@Service
@Transactional
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PermissionDao permissionDao;

    @Override
    public boolean login(String username, String password) {
        System.out.println("service_provide=======u:" + username + " p:" + password);
        if ("admin".equals(username) && "123".equals(password)){
            return true;
        }
        return false;
    }

    /**
     * 基于用户名获取User对象
     * @param username 用户名
     * @return
     */
    @Override
    public User findByUsername(String username) {

        //根据用户名获取User对象
        User user = userDao.findByUsername(username);
        //根据用户id 获取关联的角色集合
        Set<Role> roleSet = roleDao.findByUserId(user.getId());
        user.setRoles(roleSet);

        for (Role role : roleSet) {
            //根据角色id, 获取关联的权限集合
            Set<Permission> permissionSet = permissionDao.findByRoleId(role.getId());
            role.setPermissions(permissionSet);
        }

        return user;
    }
}
