package com.itheima.health.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.dao.CheckGroupDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.service.CheckGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.zookeeper.ZooDefs.OpCode.check;

/**
 * 检查组服务接口实现类
 */
@Service
@Slf4j
public class CheckGroupServiceImpl implements CheckGroupService {

    @Autowired
    private CheckGroupDao checkGroupDao;

    /**
     * 获取所有检查组数据
     */
    @Override
    public List<CheckGroup> findAll() {
        return checkGroupDao.findAll();
    }

    /**
     * 编辑检查组
     * @param checkGroup 检查组基本信息
     * @param checkitemIds 检查项选择列表
     */
    @Transactional
    @Override
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds) {
        //保存检查组信息
        checkGroupDao.edit(checkGroup);
        //删除检查组之前关联关系
        checkGroupDao.deleteCheckItemsListByCheckGroupId(checkGroup.getId());
        //保存新的关系
        for (Integer checkitemId : checkitemIds) {
            Map<String, Integer> maps = new HashMap<>();
            maps.put("checkgroup_id", checkGroup.getId());
            maps.put("checkitem_id", checkitemId);
            checkGroupDao.addCheckGroupAndCheckItem(maps);
        }
    }

    /**
     * 根据检查组ID,获取检查组数据
     * @param id 检查组id
     */
    @Override
    public CheckGroup findById(Integer id) {
        return checkGroupDao.findById(id);
    }

    /**
     * 根据检查组ID,获取选中的检查项列表
     * @param id 检查组ID
     */
    @Override
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer id) {
        return checkGroupDao.findCheckItemIdsByCheckGroupId(id);
    }

    /**
     * 分页查询
     * 1.设置分页插件初始参数
     * 2.通过dao获取page对象
     * 3.通过page对象,构建pageResult
     * @param currentPage   当前页码
     * @param pageSize      默认条数
     * @param queryString   查询条件
     */
    @Override
    public PageResult pageQuery(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage, pageSize);
        Page<CheckGroup> page = checkGroupDao.selectByCondition(queryString);
        PageResult pageResult = new PageResult(page.getTotal(), page.getResult());
        return pageResult;
    }

    /**
     * 新增
     * 1.保存检查项基本信息,获取检查项ID
     * 2.遍历检查组列表,组合检查组检查项对应Map
     * 3.保存检查组检查项数据
     */
    @Transactional
    @Override
    public void add(CheckGroup checkGroup, Integer[] checkItemIds) {
        //保存检查项基本信息,获取检查项ID
        checkGroupDao.add(checkGroup);
        log.debug("CheckGroupServiceImpl add ... checkGroup ok");

        //遍历检查组列表,组合检查组检查项对应Map
        for (Integer checkItemId : checkItemIds) {
            Map<String,Integer> map = new HashMap();
            map.put("checkgroup_id", checkGroup.getId());
            map.put("checkitem_id", checkItemId);
            //保存检查组检查项数据
            checkGroupDao.addCheckGroupAndCheckItem(map);
            log.debug("CheckGroupServiceImpl add ...addCheckGroupAndCheckItem ok: {}---{}",checkGroup.getId(), checkItemId);
        }
    }
}
