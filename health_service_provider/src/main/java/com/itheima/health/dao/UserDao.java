package com.itheima.health.dao;

import com.itheima.health.pojo.User;
import org.apache.ibatis.annotations.Param;

public interface UserDao {

    /**
     * 基于用户名获取User对象
     * @param username
     * @return
     */
    public User findByUsername(@Param("username") String username);
}
