package com.itheima.health.dao;

import com.itheima.health.pojo.Permission;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

public interface PermissionDao {

    /**
     * 基于角色id 获取所关联的权限集合
     * @param roleId
     * @return
     */
    public Set<Permission> findByRoleId(@Param("roleId") Integer roleId);
}
