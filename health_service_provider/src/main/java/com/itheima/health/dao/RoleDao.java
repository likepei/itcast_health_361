package com.itheima.health.dao;

import com.itheima.health.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

public interface RoleDao {

    /**
     * 根据用户id 获取关联的角色集合
     * @param userId
     * @return
     */
    public Set<Role> findByUserId(@Param("userId") Integer userId);
}
