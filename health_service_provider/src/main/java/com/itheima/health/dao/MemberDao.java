package com.itheima.health.dao;

import com.itheima.health.pojo.Member;
import org.apache.ibatis.annotations.Param;

/**
 * @Author likepei
 * @Date 2020/1/31 20:24
 * @Version v1.0
 * @Description 会员持久层接口
 */
public interface MemberDao {
    /**
     * 根据电话查找是否有该会员
     * @param telephone
     * @return
     */
    Member findByTeleMember(@Param("telephone") String telephone);

    /**
     * 保存会员信息
     * @param member
     */
    void add(Member member);
}
