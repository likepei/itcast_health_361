package com.itheima.health.dao;

import com.itheima.health.pojo.OrderSetting;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author likepei
 * @Date 2020/1/27 23:34
 * @Version v1.0
 * @Description 预约设置持久层接口
 */
public interface OrderSettingDao {
    /**
     * 基于预约日期获取预约设置
     * @param orderDate
     * @return
     */
    OrderSetting findByOrderDate(Date orderDate);

    /**
     * 基于预约日期更新已预约人数
     * @param orderSetting
     */
    void editReservationsByOrderDate(OrderSetting orderSetting);

    /**
     * 统计某一日期下的数据
     * @param orderDate
     * @return
     */
    long countByOrderDate(Date orderDate);

    /**
     * 基于预约日期更新预约设置
     * @param orderSetting 预约设置数据
     */
    void updateOrderSettingByOrderDate(OrderSetting orderSetting);

    /**
     * 添加预约设置
     * @param orderSetting 预约设置数据
     */
    void add(OrderSetting orderSetting);

    /**
     * 获取指定日期范围内的预约数据
     * @param date 提供月初与月末时间段
     * @return
     */
    List<OrderSetting> getOrderSettingByMonth(Map date);
}
