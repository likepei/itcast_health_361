package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.Setmeal;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 检查组持久层接口
 */
public interface CheckGroupDao {
    /**
     * 获取所有检查组数据
     */
    public List<CheckGroup> findAll();
    /**
     * 编辑检查组
     * @param checkGroup 检查组基本信息
     */
    public void edit(CheckGroup checkGroup);

    /**
     * 基于检查组ID，删除与之关联的检查项
     * @param id 检查组ID
     */
    public void deleteCheckItemsListByCheckGroupId(Integer id);

    /**
     * 根据检查组ID,获取检查组数据
     * @param id 检查组id
     */
    public CheckGroup findById(@Param("id") Integer id);

    /**
     * 根据检查组ID,获取选中的检查项列表
     * @param id 检查组ID
     */
    public List<Integer> findCheckItemIdsByCheckGroupId(@Param("id") Integer id);

    /**
     * 添加检查组
     * @param checkGroup
     */
    void add(CheckGroup checkGroup);

    /**
     * 添加检查组的检查项
     * @param map
     */
    void addCheckGroupAndCheckItem(Map<String, Integer> map);

    /**
     * 基于条件分页获取检查组列表
     * @param queryString
     * @return
     */
    Page<CheckGroup> selectByCondition(@Param("queryString") String queryString);
}
