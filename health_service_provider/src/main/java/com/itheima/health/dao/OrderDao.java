package com.itheima.health.dao;

import com.itheima.health.pojo.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author likepei
 * @Date 2020/1/31 20:47
 * @Version v1.0
 * @Description 预约订单持久层接口
 */
public interface OrderDao {

    /**
     * 保存订单
     * @param order 订单数据
     */
    void add(Order order);

    /**
     * 基于条件查询订单数据
     * @param order
     * @return
     */
    List<Order> findByCondition(Order order);

    /**
     * 根据id查询预约信息, 包括体检人信息,套餐信息
     * @param id 订单ID
     * @return
     */
    Map<String, Object> findById4Detail(@Param("id") Integer id);


}
