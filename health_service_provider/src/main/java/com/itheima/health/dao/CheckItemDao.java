package com.itheima.health.dao;

import com.github.pagehelper.Page;
import com.itheima.health.pojo.CheckItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author likepei
 * @Date 2019/12/30 16:32
 * @Version v1.0
 * @Description 检查项持久层dao接口
 */
public interface CheckItemDao {

    /**
     * 基于分页插件进行分页查询
     * @param queryString
     * @return
     */
    Page<CheckItem> selectByCondition(@Param("queryString") String queryString);

    /**
     * 保存检查项
     * @param checkItem
     */
    void add(CheckItem checkItem);

    /**
     * 根据检查项id,查询中间表记录数
     * @param checkItemId
     * @return
     */
    Long countCheckItemsById(@Param("checkItemId") Integer checkItemId);

    /**
     * 删除指定ID检查项
     * @param id
     */
    void deleteCheckItemById(@Param("id") Integer id);

    /**
     * 根据id获取检查项
     * @param id
     * @return
     */
    CheckItem findById(@Param("id") Integer id);

    /**
     * 编辑检查项
     * @param checkItem
     */
    void edit(CheckItem checkItem);

    /**
     * 获取所有检查项列表
     * @return
     */
    List<CheckItem> findAll();
}
