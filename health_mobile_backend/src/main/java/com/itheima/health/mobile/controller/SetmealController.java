package com.itheima.health.mobile.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.health.common.MessageConst;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.SetmealService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author likepei
 * @Date 2020/1/29 21:07
 * @Version v1.0
 * @Description 预约套餐服务控制器
 */
@RestController
@RequestMapping("/mobile/setmeal")
public class SetmealController {

    @Reference
    private SetmealService setmealService;

    /**
     * 根据id查询套餐信息
     */
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try {
            //获取套餐信息
            Setmeal setmeal = setmealService.findById(id);
            //成功,返回查询成功
            return new Result(true, MessageConst.QUERY_SETMEAL_SUCCESS, setmeal);
        } catch (Exception e) {
            e.printStackTrace();
            //失败,返回查询失败
            return new Result(false, MessageConst.QUERY_SETMEAL_FAIL);
        }
    }

    /**
     * 获取所有套餐信息
     * @return
     */
    @RequestMapping("/getSetmeal")
    public Result getSetmeal(){
        try {
            //获取所有套餐数据
            List<Setmeal> list = setmealService.findAll();
            //成功,返回查询成功
            return new Result(true, MessageConst.QUERY_SETMEAL_SUCCESS, list);
        } catch (Exception e) {
            e.printStackTrace();
            //失败,返回查询失败
            return new Result(false, MessageConst.QUERY_SETMEAL_FAIL);
        }
    }
}
